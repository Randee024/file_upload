<?php

class AccountModel{

    function __construct() {
        
    }

	
	/*User Initial Data*/
    function getAccounts() {
        $aColumns = array('`name`', '`username`');
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . ( $_GET['iDisplayStart'] ) . ", " .
                    ( $_GET['iDisplayLength'] );
        }


        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
				 	" . ( $_GET['sSortDir_' . $i] ) . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
		///Check Counter For Dynamic Search
        $sWhere = "";
		$check_count=0;
			$counter=0;
			$columns_shown=array();
			if(isset($_GET["show_cols"])&&$_GET["show_cols"]!="null"){
					$columns_shown=explode(",",$_GET["show_cols"]);
			}
			foreach($aColumns as $rows){
				if($_GET["bSearchable_".$counter]=="true"){
					if($_GET["sSearch_".$counter]!=""){
						$check_count++;
					}
				}
				$counter++;
			}
			if($check_count!=0){
				$sWhere = " WHERE (";
				for ($i = 0; $i < count($aColumns); $i++) {
					if($_GET["bSearchable_".$i]=="true"){
						if ($aColumns[$i] != ""&&$_GET["sSearch_".$i]!="") {
							if(count($columns_shown)>0){
								if(in_array($i,$columns_shown)>0){
										$sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch_'.$i] ) . "%' OR ";
								}else{}
							}else{
								$sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch_'.$i] ) . "%' OR ";		
							}
						}
					}
				}
				$sWhere = substr_replace($sWhere, "", -3);
				$sWhere .= ')';
			}

		
        $query = "SELECT * FROM (SELECT * FROM `accounts`)asd";

        $array = prepareTable($query . " $sWhere $sOrder $sLimit", array());
        $count = count(prepareTable($query . " $sWhere", array()));

        $countfinal = $count;


        $finalarray = array();
        foreach ($array as $rows) {
            
            $button ='
				<a href="javascript:void(0)" class="btn btn-sm btn-view btn-info" data-id="'.$rows['id'].'"><i class="glyphicon glyphicon-eye-open"></i>View Files</a>
				<a href="javascript:void(0)" class="btn btn-sm btn-delete-account btn-danger" data-id="'.$rows['id'].'"><i class="glyphicon glyphicon-trash"></i>Delete</a>
			';
			
            array_push($finalarray, array($rows["name"], $rows["username"], $button));
        }

        $jsonarray = array("sEcho" => intval($_GET['sEcho']), "iTotalRecords" => $countfinal, "iTotalDisplayRecords" => $countfinal, "aaData" => $finalarray);
        return json_encode($jsonarray);
    }
	
	public static function addAccount($data)
	{
		
		$query = '
				Call insertAccount(?,?);
		';
		$params =array(
			$data['fullname'],
			$data['username'],
		);
		
		return prepareTable($query,$params);
			
		
	}
	public static function getAccountFiles($id)
	{
		
		$query='
			SELECT
			f.*
			FROM
			`accounts` a
			Left Join `account_has_files` as af on af.`id_account` = a.`id`
			Left Join `files` as f on f.`id` = af.`id_file`
			
			Where `a`.`id` =?
		';
		
		$params =array($id);
		
		return prepareTable($query,$params);
	}
	
	public static function deleteAccount($id)
	{
		
		$query[]='
			
			Delete from `account_has_files` where `id_account` = ?; 
		
		';
		$params[] = array($id);
		
		$query[]='
			
			Delete from `accounts` where `id` = ?
		
		';
		$params[] = array($id);
		
		
		
		if(transactStatement($query,$params)){
			
			return true;
		}else{
			return false;
		}
	}
	
}
