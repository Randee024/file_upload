<?php

class FileuploadModel{

    function __construct() {
        
    }

	
	/*User Initial Data*/
    function getFiles() {
        $aColumns = array('`file_name`');
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . ( $_GET['iDisplayStart'] ) . ", " .
                    ( $_GET['iDisplayLength'] );
        }


        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
				 	" . ( $_GET['sSortDir_' . $i] ) . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
		///Check Counter For Dynamic Search
        $sWhere = "";
		$check_count=0;
			$counter=0;
			$columns_shown=array();
			if(isset($_GET["show_cols"])&&$_GET["show_cols"]!="null"){
					$columns_shown=explode(",",$_GET["show_cols"]);
			}
			foreach($aColumns as $rows){
				if($_GET["bSearchable_".$counter]=="true"){
					if($_GET["sSearch_".$counter]!=""){
						$check_count++;
					}
				}
				$counter++;
			}
			if($check_count!=0){
				$sWhere = " WHERE (";
				for ($i = 0; $i < count($aColumns); $i++) {
					if($_GET["bSearchable_".$i]=="true"){
						if ($aColumns[$i] != ""&&$_GET["sSearch_".$i]!="") {
							if(count($columns_shown)>0){
								if(in_array($i,$columns_shown)>0){
										$sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch_'.$i] ) . "%' OR ";
								}else{}
							}else{
								$sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch_'.$i] ) . "%' OR ";		
							}
						}
					}
				}
				$sWhere = substr_replace($sWhere, "", -3);
				$sWhere .= ')';
			}

		
        $query = "SELECT * FROM (SELECT * FROM `files`)asd";

        $array = prepareTable($query . " $sWhere $sOrder $sLimit", array());
        $count = count(prepareTable($query . " $sWhere", array()));

        $countfinal = $count;


        $finalarray = array();
        foreach ($array as $rows) {
            
            $button ='
				<a href="'.$rows['file_path'].$rows['file_name'].'" target="_blank" class="btn btn-sm btn-view btn-info" data-id="'.$rows['id'].'"><i class="glyphicon glyphicon-eye-open"></i>View Image</a>
				<a href="javascript:void(0)" class="btn btn-sm btn-delete btn-danger" data-id="'.$rows['id'].'"><i class="glyphicon glyphicon-trash"></i>Delete</a>
			';
			
            array_push($finalarray, array($rows["file_name"],$button));
        }

        $jsonarray = array("sEcho" => intval($_GET['sEcho']), "iTotalRecords" => $countfinal, "iTotalDisplayRecords" => $countfinal, "aaData" => $finalarray);
        return json_encode($jsonarray);
    }
	
	public static function uploadAccountFiles($data,$fileName,$path,$originalFileName)
	{
		
		$query[]="
			set @`fileID`:=(SELECT `AUTO_INCREMENT`
				FROM  INFORMATION_SCHEMA.TABLES
				WHERE TABLE_SCHEMA = 'file_upload'
				AND   TABLE_NAME   = 'files');
				";
		
		$params[] = array();
		
		$query[] = '
				Insert into `files` (`file_name`,`file_path`,`original_file_name`) VALUES(?,?,?);
		';
		$params[] =array(
			$fileName,
			$path,
			$originalFileName,
		);
		
		
		$query[] = '
				Insert into `account_has_files` (`id_file`,`id_account`) VALUES((select @`fileID`),?);
		';
		$params[] =array(
			$data['account_id']
		);
		
		
		if(transactStatement($query,$params)){
			
			return true;
			
		}else{
			
			return false;
			
		}
	}
	
	
	public static function deleteFile($id)
	{
		
		
		$query[]='
			
			Delete from `account_has_files` where `id_file` = ?
		
		';
		$params[] = array($id);
		
		$query[]='
			
			Delete from `files` where `id` = ?
		
		';
		$params[] = array($id);
		
		
		
		if(transactStatement($query,$params)){
			
			return true;
		}else{
			return false;
		}
		
	}
	
	
}
