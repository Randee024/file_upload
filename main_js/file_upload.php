
<script>
 
 
	
	
	
	var myDropzone = new Dropzone('#myDropZone', {
		url: "/fileUpload",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 6, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10 //for upload all at the same time
	});
	
	myDropzone.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
 $('body').on('click','.btn-upload',function(e){
	
	var self = $('#form-upload');
	
	$.ajax({
		url:'/addAccount',
		data:self.serialize(),
		type:'POST',
		dataType:'json',
		success:function(res){
				
				if(res.status){
					$('#account_id').val(res.id);
					myDropzone.processQueue();
					var r = confirm("Account and files has been saved.");
					if (r == true) {
						 window.location.replace("/");
					}else{
						 window.location.replace("/");
					}
				}
				
		}
		
	});
	
 });
 

Dropzone.autoDiscover = false;
</script>