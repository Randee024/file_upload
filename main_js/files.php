
<script>
 
	$('body').on('click','.btn-delete',function(e){
			
		var ID = $(this).data('id');
		
		$.ajax({
			url:'/deleteFile',
			data:{id:ID},
			type:'POST',
			dataType:'json',
			success:function(res){
				if(res){
					if (confirm("File has been deleted.") {
						 window.location.replace("/files");
					}else{
						 window.location.replace("/files");
					}
				}else{
					alert('Failed to delete file.');
				}
			}
		});
		
	});
	
	$('#table_files').dataTable({
		"bProcessing":true,
		"bServerSide":true,
		"sAjaxSource":"/getFiles",
		"aoColumnDefs":[{
			"bSortable":false,"aTargets":[1]
			}]
	});

</script>