
<script>
 
	$('body').on('click','.btn-view',function(e){
		var ID = $(this).data('id');
		$('#account_files').html('');
		$.ajax({
			url:'/getAccountFiles',
			data:{id:ID},
			type:'POST',
			dataType:'json',
			success:function(res){
				
				$.each(res,function(i,field){
					
					$('#account_files').append('<tr><td>'+field.original_file_name+'</td><td><a href="'+field.file_path+field.file_name+'" class="btn btn-sm btn-info" target="_blank"><i class="glyphicon glyphicon-eye-open"></i> View File</a><a href="javascript:void(0)" class="btn btn-sm btn-danger btn-delete-file" data-id="'+field.id+'"><i class="glyphicon glyphicon-delete"></i>Delete</a></td></tr>');
					
				});
				$('#accountFiles').modal({
					backdrop:'static',
					keyboard:false
				})
			}
		});
		
	});

	
	$('body').on('click','.btn-delete-file',function(e){
			
		var ID = $(this).data('id');
		
		$.ajax({
			url:'/deleteFile',
			data:{id:ID},
			type:'POST',
			dataType:'json',
			success:function(res){
				if(res){
					if (confirm("File has been Deleted.")) {
						 window.location.replace("/accounts");
					}else{
						 window.location.replace("/accounts");
					}
				}else{
					alert('Failed to delete file.');
				}
			}
		});
		
	});
	
	$('body').on('click','.btn-delete-account',function(e){
			
		var ID = $(this).data('id');
		
		$.ajax({
			url:'/deleteAccount',
			data:{id:ID},
			type:'POST',
			dataType:'json',
			success:function(res){
				if(res){
					if (confirm("Account has been deleted.")) {
						 window.location.replace("/accounts");
					}else{
						 window.location.replace("/accounts");
					}
				}else{
					alert('Failed to delete account.');
				}
			}
		});
		
	});
	
	$('#table_accounts').dataTable({
		"bProcessing":true,
		"bServerSide":true,
		"sAjaxSource":"/getAccounts",
		"aoColumnDefs":[{
			"bSortable":false,"aTargets":[2]
			}]
	});

</script>