CREATE DATABASE  IF NOT EXISTS `product_supplier` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `product_supplier`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: product_supplier
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Product 1'),(2,'Product 2'),(3,'Product 3'),(4,'Product 4'),(5,'Product 5');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` VALUES (1,'Supplier 1'),(2,'Supplier 2'),(3,'Supplier 3'),(4,'Supplier 4'),(5,'Supplier 5');
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_has_product`
--

DROP TABLE IF EXISTS `supplier_has_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_has_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_supplier` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `price` double(11,2) DEFAULT '0.00',
  `modified_on` timestamp NULL DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_has_product`
--

LOCK TABLES `supplier_has_product` WRITE;
/*!40000 ALTER TABLE `supplier_has_product` DISABLE KEYS */;
INSERT INTO `supplier_has_product` VALUES (5,1,2,400.00,'2017-07-05 03:58:18',1),(6,2,2,200.00,NULL,NULL),(7,3,2,300.00,NULL,NULL),(8,4,2,300.00,NULL,1),(9,5,2,500.00,NULL,NULL),(10,1,3,622.00,'2017-07-05 03:58:19',1),(11,2,3,100.00,NULL,NULL),(12,3,3,250.00,NULL,NULL),(13,4,3,300.00,NULL,NULL),(14,5,3,150.00,NULL,NULL),(15,1,4,356.00,'2017-07-05 03:58:19',1),(16,2,4,100.00,NULL,NULL),(17,3,4,200.00,NULL,NULL),(18,4,4,200.00,NULL,NULL),(19,5,4,140.00,NULL,NULL),(20,1,5,300.00,'2017-07-05 03:58:19',1),(21,2,5,1200.00,NULL,NULL),(22,3,5,1100.00,NULL,NULL),(23,4,5,1140.00,NULL,NULL),(24,5,5,1150.00,NULL,NULL),(25,2,1,1.00,'2017-07-06 05:49:04',5),(26,1,1,2.00,'2017-07-06 05:49:04',5),(27,5,1,3.00,'2017-07-06 05:49:04',5),(28,3,1,4.00,'2017-07-06 05:49:04',5),(29,4,1,5.00,'2017-07-06 05:49:04',5);
/*!40000 ALTER TABLE `supplier_has_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_has_product_history`
--

DROP TABLE IF EXISTS `supplier_has_product_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_has_product_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_supplier_product` int(11) DEFAULT NULL,
  `price` double(11,2) DEFAULT NULL,
  `issued_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `issued_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_has_product_history`
--

LOCK TABLES `supplier_has_product_history` WRITE;
/*!40000 ALTER TABLE `supplier_has_product_history` DISABLE KEYS */;
INSERT INTO `supplier_has_product_history` VALUES (2,25,100.00,'2017-07-06 05:49:04',5),(3,26,122.00,'2017-07-06 05:49:04',5),(4,27,100.00,'2017-07-06 05:49:04',5),(5,28,122.00,'2017-07-06 05:49:04',5),(6,29,56.00,'2017-07-06 05:49:04',5);
/*!40000 ALTER TABLE `supplier_has_product_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `iduser_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Account1','Admin1','1234'),(4,'Account','AdminAdmin','1234'),(5,'Sample','admin','1234');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-06 13:51:28
