	
	<div class="content-wrapper">
		<div class="container">
			<section class="content" id="section" data="50"> 
				<div class="row">
					<div class="col-md-12">
						<div class="box box-default">
							<div class="box-header">
								<h3 class="box-title">File Upload</h3>
							</div>
							<div class="box-body">
								<form id="form-upload" enctype="multipart/form-data" action="/try" method="POST">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6 form-group">
												<input type="text" class="form-control" name="fullname" placeholder="Fullname"/>
											</div>
											<div class="col-md-6 form-group">
												<input type="text" class="form-control" name="username" placeholder="Username"/>
											</div>
										</div>
									</div>
									
								
								</form>
									<div class="row">
										<div class="col-md-12">
											<form id="myDropZone" class="dropzone" method="POST">
												<input type="hidden" name="account_id" id="account_id" />
											</form>
										</div>
									</div>
								<div class="row">
									<div class="col-md-12">
										</br>
										<a href="javascript:void(0)" class="btn btn-success pull-right btn-upload"> Submit </a>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>