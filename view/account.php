	
	<div class="content-wrapper">
		<section class="content" id="section" data="50"> 
			<div class="row">
				<div class="col-md-12">
					<div class="box box-default">
						<div class="box-header">
							<h3 class="box-title">Accounts</h3>
						</div>
						<div class="box-body">
							<table id="table_accounts" class="table table-hover table-bordered">
								<colgroup>
									<col width="35%">
									<col width="35%">
									<col width="30%">
								</colgroup>
								<thead>
									<tr>
										<th>Name</th>
										<th>Username</th>
										<th>Actions</th>
									</tr>
									
								</thead>
							</table>
							
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
<?php
	include 'modal/account_files.php';
?>