</div>
</main>
<script src="./public/js/modalEffects.js"></script>
<script src="./public/js/main.js"></script>
<script>


    $(".toggle-side").click(function () {
        $(".cd-side-nav").toggle();
        //$(".cd-main-content .content-wrapper").css('margin-left','0');
        //alert($("#section").attr('data'));

        if ($("#section").attr('data') == '50') {
            $("#section").attr('data', '100');
            $("#section").css('width', '100%');
            $(".cd-main-content .content-wrapper").css({'margin-left': '0', 'padding-left': '0', 'padding-right': '0'});
        } else {
            $("#section").attr('data', '50');
            $("#section").css('width', '100%');
            $(".cd-main-content .content-wrapper").css({'margin-left': '110px', 'padding-left': '5px', 'padding-right': '5px'});
        }
    });

    $(document).ready(function () {
        $('.header-select').multiselect({
            includeSelectAllOption: true,
            selectAllValue: 'select-all-value'
        });
        $('#header-select2').multiselect({
            includeSelectAllOption: true,
            selectAllValue: 'select-all-value'
        });
        $('.col-select').multiselect({
            includeSelectAllOption: true,
            selectAllValue: 'select-all-value'
        });
        $('#col-select2').multiselect({
            includeSelectAllOption: true,
            selectAllValue: 'select-all-value'
        });	
    });
    $(document).ready(function () {

        var defaults = {
            containerID: 'toTop',
            containerHoverID: 'toTopHover',
            scrollSpeed: 1200,
            easingType: 'linear'
        };

        $().UItoTop({easingType: 'easeOutQuart'});

    });

    $(".sidebar-toggle").click(function (e) {
        e.preventDefault();
        $("nav.sidebar, .main-header, .content-wrapper").toggleClass("active");
    });

    $('#datetimepicker1').datetimepicker();
    $('.datepicker').datepicker({
		format:"yyyy-mm-dd",
		defaultViewDate: new Date(),
		todayBtn : 'linked',
        keyboardNavigation:true,
        autoclose: true,
		todayHighlight : true
		
		});
    $('#datepicker1').datepicker({
		format:"yyyy-mm-dd",
		defaultViewDate: new Date()
		});
    $('#timepicker1').timepicker();
    $('#timepicker2').timepicker();



    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();
    });
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });


    $('.language').on('click', function () {
        chooseLanguage($(this).attr("value"));
    });

    function chooseLanguage(id) {

        $.post("/changeLanguage", {id: id}, function (data) {
            if (data === 1) {
                document.location.reload();
            }

        }, 'json');
    }
    window.onkeypress = function (event) {

        if (event.keyCode == 13) {
            $('#sin').click();
        }

    }
   
   function mobileViewUpdate() {
    var viewportWidth = $(window).width();
    if (viewportWidth > 1910) {
        $(".table-scroll").removeClass("table-responsive");
    }
}

$(window).load(mobileViewUpdate);
$(window).resize(mobileViewUpdate);



// CLEARABLE INPUT
function tog(v){return v?'addClass':'removeClass';} 
$(document).on('input', '.clearable', function(){
    $(this)[tog(this.value)]('x');
}).on('mousemove', '.x', function( e ){
    $(this)[tog(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]('onX');
}).on('touchstart click', '.onX', function( ev ){
    ev.preventDefault();
    $(this).removeClass('x onX').val('').change();
});




 
 
 
 /**Table Codes**/
	/*Page Info Loading*/
	/* $(window).load(function(){
		loadPageInfo('tabledata');
	}); */

	/**Loads Page Info (Page number and Page count)**/
	var global_pages=0;
	function loadPageInfo(table){
			var info = $('#'+table).DataTable().page.info();
			global_pages = info.pages;
			$('.total_pages[table='+table+']').html(info.pages);
			$('.viewcount[table='+table+']').html("View "+parseInt(info.start+1)+"-"+info.end+" of "+info.recordsTotal);
			$('.pageset[table='+table+']').val(info.page+1);
	}
	
	/*Column Show*/
    $('select.select_table').on('change', function (e) {
		var cols=$('#table_cols[table='+$(this).attr('table')+']').val();
        var col = $(this).val();
        if (col == null) {
            $('#'+$(this).attr('table')).DataTable().columns().visible(true, true);
        } else {
            $('#'+$(this).attr('table')).DataTable().columns().visible(false, false);
            $('#'+$(this).attr('table')).DataTable().column($(this).val()).visible(true, true);
        }
		$('#'+$(this).attr('table')).DataTable().ajax.url($('#'+$(this).attr('table')).DataTable().ajax.url()+'?show_cols='+cols).load();
    });
	
	/*Column Search*/
    var global_value="";
		/**For Search Button Click**/
		$('.srchbtn').on('click',function(){
			var value=$('.srch_value[table='+$(this).attr('table')+']').val();
			var cols=$('.search_col[table='+$(this).attr('table')+']').val();
			if(global_value!==""){
				$('#'+$(this).attr('table')).DataTable().columns(global_value).search("").draw();
			}
			
			$('#'+$(this).attr('table')).DataTable().columns(cols).search( value ).draw();
			global_value=cols;
			loadPageInfo($(this).attr('table'));
		});
		
		/**For KeyPress Enter**/
		$('.srch_value').on('keydown',function(e){
			if (e.keyCode == 13) {
				var value=$('.srch_value[table='+$(this).attr('table')+']').val();
				var cols=$('.search_col[table='+$(this).attr('table')+']').val();
				if(global_value!==""){
					$('#'+$(this).attr('table')).DataTable().columns(global_value).search("").draw();
				}
				
				$('#'+$(this).attr('table')).DataTable().columns(cols).search( value ).draw();
				global_value=cols;
			}
			loadPageInfo($(this).attr('table'));
		});


	/*Page Number of shown data*/
	$('.row_limit').on('change',function(){
		var length = $(this).val();
		$('#'+$(this).attr('table')).DataTable().page.len( length ).draw();
		loadPageInfo($(this).attr('table'));
	});
	
	/*Page Change (next/prev)*/
	$('.Last_Page').on('click',function(){
		$('#'+$(this).attr('table')).dataTable().fnPageChange( 'last' );
		loadPageInfo($(this).attr('table'));
	});
	$('.Next_Page').on('click',function(){
		$('#'+$(this).attr('table')).dataTable().fnPageChange( 'next' );
		loadPageInfo($(this).attr('table'));
	});
	$('.Prev_Page').on('click',function(){
		$('#'+$(this).attr('table')).dataTable().fnPageChange( 'previous' );
		loadPageInfo($(this).attr('table'));
	});
	$('.First_Page').on('click',function(){
		$('#'+$(this).attr('table')).dataTable().fnPageChange( 'first' );
		loadPageInfo($(this).attr('table'));
	});

	/*Page Change Set*/
	/**On KeyPress Enter**/
	$('.pageset').on('keydown',function(e){
		if (e.keyCode == 13) {
			if($(this).val()<0){
				$('#'+$(this).attr('table')).dataTable().fnPageChange(global_pages-global_pages);
			}else if($(this).val()>=0&&$(this).val()<=global_pages){
				$('#'+$(this).attr('table')).dataTable().fnPageChange(parseInt($(this).val()-1));
			}else if($(this).val()>global_pages){
				$('#'+$(this).attr('table')).dataTable().fnPageChange(global_pages-1);
			}
				loadPageInfo($(this).attr('table'));
		}
	});

 
	///Allow numbers, dot, backspace, delete, left and right arrow keys
	scoreme();
	function scoreme(){
		 $('.score').keypress(function(event) {
			var key = window.event ? event.keyCode : event.which;

			if (event.keyCode == 8 || event.keyCode == 46
			 || event.keyCode == 37 || event.keyCode == 39) {
				return true;
			}
			else if ( (event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) ) {
				return false;
			}
			else return true;
		});
	}
</script>
<script src="https://rawgit.com/enyo/dropzone/master/dist/dropzone.js"></script>
<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
</body>
</html>
