<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title><?= $invtitle ?></title>
        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <link rel="stylesheet" href="./public/css/bootstrap.min.css" />
        <link rel="stylesheet" href="./public/font-awesome/4.2.0/css/font-awesome.min.css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="./public/css/style.css"  />
        <script src="./public/js/jquery.2.1.1.min.js"></script>
        <script src="./public/js/bootstrap.min.js"></script>
    </head>
    <body>