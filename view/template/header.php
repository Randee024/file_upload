<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>File Upload</title>
        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <link rel="stylesheet" href="./public/css/bootstrap.min.css" />
        <link rel="stylesheet" href="./public/font-awesome/4.2.0/css/font-awesome.min.css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="./public/css/style.css"  />
        <link rel="stylesheet" href="./public/css/slider.css"  />
        <link rel="stylesheet" href="./public/css/datepicker.min.css" />
        <link type="text/css" href="./public/css/bootstrap-timepicker.min.css" />
        <link rel="stylesheet" href="./public/css/bootstrap-datetimepicker.min.css" />
        <link rel="stylesheet" href="./public/css/jquery.dataTables.min.css"  />
        <link rel="stylesheet" href="./public/css/bootstrap-multiselect.css" type="text/css"/>
        <link rel="stylesheet" href="./public/css/dropzone.css" type="text/css"/>
        <link href="./public/css/bootstrap-toggle.min.css" rel="stylesheet">  
        <link href="./public/css/bootstrap-tagsinput.css" rel="stylesheet">
        <script src="./public/js/jquery.2.1.1.min.js"></script>
        <script src="./public/js/jquery.menu-aim.js"></script>
        <script src="./public/js/dropzone.js"></script>
        <script src="./public/js/jquery.dataTables.min.js"></script>
        <script src="./public/js/moment.min.js"></script>
        <script src="./public/js/bootstrap-datepicker.min.js"></script>
        <script src="./public/js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="./public/js/bootstrap-timepicker.min.js"></script>
        <script type="text/javascript" src="./public/js/bootstrap-multiselect.js"></script>
        <script type="text/javascript" src="./public/js/bootstrap-tagsinput.min.js"></script>
        <script src="./public/js/classie.js"></script> 
        <script src="./public/js/bootstrap-toggle.min.js"></script>
        <script src="./public/js/modernizr.custom.js"></script>
        <script src="./public/js/move-top.js"></script>
        <script src="./public/js/easing.js"></script>
        <script src="./public/js/bootstrap.min.js"></script>
    </head>
    <body class="skin-blue">
        <div class="wrapper">
            <header class="cd-main-header">
                <a href="/index" data-toggle="tooltip" data-placement="bottom" title="Inventory System" class="cd-logo"><img src="./public/images/logo.png" class="img-responsive"/></a>
                <button type="button" class="toggle-side" data-toggle="collapse" data-target="#cd-nav">
                    <i class="fa fa-bars"></i>
                </button>
                <a href="#0" class="cd-nav-trigger"><span></span></a>
                <nav class="cd-nav">
                    <ul class="cd-top-nav">
                        <li class="has-children language"><a data-toggle="tooltip" data-placement="bottom" title="Choose Language"><i class="fa fa-language"></i></a>
                            <ul>
                                <li><a class="lang" id="english" href="javascript:void(0)" onclick="chooseLanguage(1)" value="1">
                                        <div class="clearfix">
                                            <span class="pull-left">english</span>
                                            <span class="pull-right"><img src="./public/images/usa-flag.png" class="img-responsive" /></span>
                                        </div>
                                    </a>
                                </li>
                                <li><a class="lang" id="chinese" href="javascript:void(0)" onclick="chooseLanguage(2)" value="2">
                                        <div class="clearfix">
                                            <span class="pull-left">simplified chinese</span>
                                            <span class="pull-right"><img src="./public/images/china-flag.png" class="img-responsive" /></span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-children account">
                            <a href="#0">
                                <img src="./public/images/avatar2.jpg" alt="avatar">
                                anne smith
                            </a>
                            <ul>
                                <li class="userheader">
                                    <img src="./public/images/avatar2.jpg" class="img-responsive img-circle" />
                                    <p>anne smith&nbsp;-&nbsp;admin</p>
                                </li>
                                <li class="userfooter">                               
                                    <a href="/login"><button class="btn btn-default btn-flat">Sign out</button></a>
									
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </header>
            <main class="cd-main-content">
                <nav class="cd-side-nav">
                    <ul>
                        <li class="has-children overview">
                            <a href="#0"><img src="./public/images/info.png"/><span></span></a>
							 <ul class="info-menu">
                                <div class="dropdownlist">
                                    <div class="col-md-12 no-padding">
										<div class="col-md-4">
                                            <div class="catbox">
                                                <h3>File Upload</h3>
                                                <li id="nav_upload"><a href="/">upload files</a><button type="button" class="btn btn-xxs btn-primary"><i class="fa fa-plus"></i></button></li>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="catbox">
                                                <h3>Accounts</h3>
                                                <li id="nav_accounts"><a href="/accounts">list of accounts</a><button type="button" class="btn btn-xxs btn-primary"><i class="fa fa-plus"></i></button></li>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <h3>Files</h3>
                                            <li id="nav_files"><a href="/files">list of files</a><button type="button" class="btn btn-xxs btn-primary"><i class="fa fa-plus"></i></button></li>
                                        </div>
                                    </div>                                    
                                </div>
                            </ul>
                    </ul> 
                </nav>
<style>
.clearable{
  background: #fff url(http://i.stack.imgur.com/mJotv.gif) no-repeat right -10px center;
  border: 1px solid #999;
  padding: 3px 18px 3px 4px;     /* Use the same right padding (18) in jQ! */
  border-radius: 3px;
  transition: background 0.4s;
}
.clearable.x  { background-position: right 5px center; } /* (jQ) Show icon */
.clearable.onX{ cursor: pointer; }              /* (jQ) hover cursor style */
.clearable::-ms-clear {display: none; width:0; height:0;} /* Remove IE default X */
</style>