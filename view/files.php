	
	<div class="content-wrapper">
		<section class="content" id="section" data="50"> 
			<div class="row">
				<div class="col-md-12">
					<div class="box box-default">
						<div class="box-header">
							<h3 class="box-title">Files</h3>
						</div>
						<div class="box-body">
							<table id="table_files" class="table table-hover table-bordered">
								<colgroup>
									<col width="50%">
									<col width="50%">
								</colgroup>
								<thead>
									<tr>
										<th>File Name</th>
										<th>Actions</th>
									</tr>
									
								</thead>
							</table>
							
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>