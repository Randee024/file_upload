<?php
$router->map('GET', '/', function() {

    $tablename="file_upload";
	include linkPage("templateload");
	
});



$router->map('GET', '/getAccounts', function() {

	$accountModel = new AccountModel;
	
	echo $accountModel->getAccounts();
});

$router->map('GET', '/getFiles', function() {

	$fileModel = new FileuploadModel;
	
	echo $fileModel->getFiles();
});
$router->map('POST', '/fileUpload', function() {

	$fileModel = new FileuploadModel;
	
	
	$target_dir = "upload/";
	
	$temp = array();
	
	if(isset($_FILES['file']) && !empty($_FILES['file'])){
		$temp = $_FILES['file']['tmp_name'];
	}
	
	foreach($_FILES['file']['name'] as $key=> $name){
	
		$target_file = $target_dir . basename($name);
		$return= false;
		
		if (move_uploaded_file($temp[$key], $target_dir.time().$key.$name)) {
			
			$fileName = time().$key.$name;
			
			$return = $fileModel->uploadAccountFiles($_POST,$fileName,$target_dir,$name);
			
			
		}
	
	}
	echo json_encode($return);
});


$router->map('POST', '/addAccount', function() {

   $accoutModel = new AccountModel;
   
   $return = array();
   
   $result = $accoutModel->addAccount($_POST);
   
   if(!empty($result)){
   
		$return['status']= true;
		$return['id'] = $result[0]['ID'];
	}else{
	
		$return['status']= false;
	}
	
   echo json_encode($return);
   
});


$router->map('POST', '/getAccountFiles', function() {

   $accoutModel = new AccountModel;
      
   $result = $accoutModel->getAccountFiles($_POST['id']);
 
   echo json_encode($result);
   
});

$router->map('POST', '/deleteFile', function() {

   $fileModel = new FileuploadModel;
      
   $result = $fileModel->deleteFile($_POST['id']);
 
   echo json_encode($result);
   
});

$router->map('POST', '/deleteAccount', function() {

   $accountModel = new AccountModel;
      
   $result = $accountModel->deleteAccount($_POST['id']);
 
   echo json_encode($result);
   
});
$router->map('GET', '/accounts', function() {

    $tablename="account";
	include linkPage("templateload");
	
});

$router->map('GET', '/files', function() {

    $tablename="files";
	include linkPage("templateload");
	
});

?>
