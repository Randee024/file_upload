<?php

$match = $router->match();

if ($match && is_callable($match['target'])) {
    call_user_func_array($match['target'], $match['params']);
} else if ($match) {
    require $match['target'];
} else {
    
  //  header("HTTP/1.0 404 Not Found");
    $error="ERROR 404.Page not Found";
    $message="it appears that the page your are looking for doesn't exist anymore or was moved";
    include linkPage("error");
}
  
