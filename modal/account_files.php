<div id="accountFiles" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header alert-info">
				<h4 class="modal-title">Account Files</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<table class="table table-hover table-bordered">
							<colgroup>
								<col width="50%">
								<col width="50%">
							</colgroup>
							<thead>
								<th>Filename</th>
								<th>Actions</th>
							</thead>
							<tbody id="account_files">
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default"  data-dismiss="modal" ><i class="glyphicon glyphicon-remove-sign"></i>Cancel</button>
			</div>
		</div>

	</div>
</div>